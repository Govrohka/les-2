using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tests : MonoBehaviour
{


    [SerializeField] private GameObject _endPoint = null;
    private bool _end = false;
    private bool _start = false;
    private Vector3 _endPotition;
    private float _speed = 2f;
    private Vector3 _direction;
    private Vector3 _startPotition;
    private float _epsilon = 0.5f;
    void Start()
    {
        _startPotition = transform.position;
        _endPotition = _endPoint.transform.position;

    }

    void Update()
    {
        float distance2 = Vector3.Distance(transform.position, _startPotition);
        float distance = Vector3.Distance(transform.position, _endPotition);
        if (_end == false)
        {


            if ((distance >= _epsilon))
            {
                if ((_start == true))
                {
                    _direction = _endPoint.transform.position - transform.position; //������ ����� ������� ���� �� ���������� �������� ���� �������� ���� ����������!
                    _direction.Normalize();
                    Vector3 newPosition = transform.position + (_direction * _speed * Time.deltaTime);

                    transform.position = newPosition;
                }

            }
            else
            {
                _end = true;
                _start = false;
            }
        }
        else
        {
            if ((distance2 >= _epsilon))
            {
                if ((_start == true))
                {

                    _direction = _startPotition - transform.position; //������ ����� ������� ���� �� ���������� �������� ���� �������� ���� ����������!
                    _direction.Normalize();
                    Vector3 newPosition = transform.position + (_direction * _speed * Time.deltaTime);

                    transform.position = newPosition;

                }
            }
            else
            {
                _end = false;
                _start = false;
            }

        }



    }
    private void OnMouseDown()
    {
        _start = true;
      
    }
}
