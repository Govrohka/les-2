using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePhisiks : MonoBehaviour
{
    [SerializeField] private Transform _shotdir = null;
    [SerializeField] private GameObject _ammo = null;
    [SerializeField] private float _speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hello World!");
    }

    // Update is called once per frame
    void Update()
    {
        

        
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.forward * -_speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0,100 * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, -100 * Time.deltaTime, 0);
        }

    }

    private void OnMouseDown()
    {
        Instantiate(_ammo, _shotdir.position, transform.rotation);
    }


}
