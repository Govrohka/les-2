using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Updatetests : MonoBehaviour
{
    [SerializeField] private GameObject _secondCube = null;
    private Vector3 _endPotition;
    private Vector3 _startPotition;
    private Vector3 _direction;
    private float _speed = 2f;
    private float distance;
    private float epsilon = 0.5f;
    private bool _startMoving = false;
    private bool _onEnd = false;
    private Vector3 _finalPotition;
    void Start()
    {
        _endPotition = _secondCube.transform.position;
        _startPotition = transform.position;
        _finalPotition = _endPotition;
    }

    void Update()
    {

        Analise();
        Moving();
    }

    private void Analise()
    {
        distance = Vector3.Distance(transform.position, _finalPotition);

        


        _onEnd = distance <= epsilon ? true : false;

        if (_onEnd == true)
        {
            _startMoving = false;

            _finalPotition = _finalPotition == _endPotition ? _startPotition : _endPotition;
        }
    }


    private void Moving()
    {
        if (_startMoving == true)
        {
            _direction = _finalPotition - transform.position; //������ ����� ������� ���� �� ���������� �������� ���� �������� ���� ����������!
            _direction.Normalize();
            Vector3 newPosition = transform.position + (_direction * _speed * Time.deltaTime);
            transform.position = newPosition;
        }
    }


    void OnMouseDown()
    {
        _startMoving = true;
    }

}